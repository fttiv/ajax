var form = document.getElementById('sending-form'),
    emailF = document.getElementById('emailF'),
    error = document.getElementById('err-place'),
    passwordF = document.getElementById('passwordF'),
    loader = document.getElementById('loader'),     
    submit = document.getElementById('submit'),
    profile = document.getElementById('profile'),
    profileAvatar = document.getElementById('profileAvatar'),
    profileFullName = document.getElementById('profileFullName'),
    profileCountry = document.getElementById('profileCountry'),
    profileHobbies = document.getElementById('profileHobbies'),
    signOut = document.getElementById('signOut');


signOut.addEventListener('click',function(e) {
      loader.style.display = 'none';
      profile.style.display = 'none';
      form.style.display = 'block';
      submit.style.display = 'block';
      emailF.style.display = 'block';
      passwordF.style.display = 'block';
      error.style.display = 'none';  
});


form.addEventListener('submit', function(e) {
     var email = document.getElementById("email").value,
      password = document.getElementById("password").value;

        
     var xhr = new XMLHttpRequest();
     var body = 'email=' + encodeURIComponent(email) +
                '&password=' + encodeURIComponent(password);

     console.log(body);
     xhr.open('POST', 'http://netology-hj-ajax.herokuapp.com/homework/login_json', true);
    
     xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

     xhr.addEventListener('loadstart', onLoadStart); 
     xhr.send(body);
     xhr.onload = function () {
    // вывести результат
          if (xhr.status != 200) {
         // обработать ошибку
              console.log( xhr.status + ': ' + xhr.statusText ); // 404: Not Found
              loader.style.display = 'none';
              submit.style.display = 'block';
              emailF.style.display = 'block';
              passwordF.style.display = 'block';
              error.style.display = 'block';
              error.innerHTML = 'Возникал ошибка: ' + xhr.status + ': ' + xhr.statusText;
          } else {         
             console.log( xhr.responseText ); // responseText — текст ответа.
             var data = JSON.parse( xhr.responseText);
             form.style.display = 'none';
              profile.style.display = 'block';
              profileAvatar.src = data.userpic;
              profileFullName.innerHTML = data.name + ' ' + data.lastname;
              profileCountry.innerHTML = data.country;
              profileHobbies.innerHTML =  data.hobbies.join(', ');
          }
        
     }
     xhr.onerror = function () {
         // обработать ошибку
         error.innerHTML = 'Возникал ошибка';
     }

     e.preventDefault();
     return false;
});


function onLoadStart() {
    loader.style.display = "block";
    submit.style.display = "none";
    emailF.style.display = "none";
    passwordF.style.display = "none";
    error.style.display = "none";
}
